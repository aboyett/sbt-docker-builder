#!/usr/bin/env sh
CI_PROXYREPO_SENTINEL="${HOME}/.ci-proxyrepo-stamp"
SBT_REPOS_FILE="${HOME}/.sbt/repositories"

# setup sbt proxy repos if either ivy or maven url is set
if [ -n "${SBT_MAVEN_PROXY}" -o -n "${SBT_IVY_PROXY}" ] ; then
    # add CLI args for sbt to use proxy repos
    export SBT_EXTRAS_OPTS="${SBT_EXTRAS_OPTS} -Dsbt.override.build.repos=true"

    # setup sbt repositories file if not already setup
    if [ ! -f "${CI_PROXYREPO_SENTINEL}" ] ; then
        echo "[repositories]" > ${SBT_REPOS_FILE}

        if [ -n "${SBT_MAVEN_PROXY}" ] ; then
            echo "Injecting maven proxy repo into sbt configuration"
            echo "maven-proxy: ${SBT_MAVEN_PROXY}" >> ${SBT_REPOS_FILE}
        fi

        # reference: http://www.scala-sbt.org/0.13/docs/Proxy-Repositories.html
        if [ -n "${SBT_IVY_PROXY}" ] ; then
            echo "Injecting ivy proxy repo into sbt configuration"
            echo "ivy-proxy: ${SBT_IVY_PROXY}, [organization]/[module]/(scala_[scalaVersion]/)(sbt_[sbtVersion]/)[revision]/[type]s/[artifact](-[classifier]).[ext]" >> ${SBT_REPOS_FILE}
        fi

        touch ${CI_PROXYREPO_SENTINEL}
    fi
else
    echo "Skipping proxy repository configuration. Neither \$SBT_IVY_PROXY or \$SBT_MAVEN_PROXY are set."
fi

