## sbt-builder ##
This repo is for creating the sbt-builder image that is used for compiling
Scala projects. The image pulls Paul Phillips' sbt-extra and runs sbt commands
for the Gitlab CI system. To configure ivy and maven Artifactory caching,
the Gitlab CI/CD secret variables `SBT_IVY_PROXY` and `SBT_MAVEN_PROXY`
need to be set in the project repository that uses Artifactory.
